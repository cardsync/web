import { Application } from "stimulus"
import UserCardConditionShowController from "packs/controllers/user_card_condition/show_controller"

describe("UserCardConditionShowController", () => {
   beforeEach(() => {
      document.body.innerHTML = `<div data-controller="show">
          <div class="tabs is-large is-boxed">
            <ul>
              <li data-target="show.ebayButton" class="is-active"><a id="ebay-button" data-action="show#showEbay">eBay Activity</a></li>
              <li data-target="show.shopifyButton"><a id="shopify-button" data-action="show#showShopify">Shopify Activity</a></li>
            </ul>
          </div>
         <div class="box" data-target="show.ebay" id="ebay"></div>
         <div class="box is-hidden" data-target="show.shopify" id="shopify"></div>
      </div >`;

      const application = Application.start()
      application.register("show", UserCardConditionShowController)
   })

   describe("when shopify button is clicked", () => {
      beforeEach(() => {
         document.getElementById("shopify-button").click()
      })

      it("hides the ebay box", () => {
         expect(document.getElementById("ebay").classList).toContain("is-hidden")
      })

      it("shows the shopify box", () => {
         expect(document.getElementById("shopify").classList).not.toContain("is-hidden")
      })

      describe("when ebay button is clicked", () => {
         beforeEach(() => {
            document.getElementById("ebay-button").click()
         })

         it("shows the ebay box", () => {
            expect(document.getElementById("ebay").classList).not.toContain("is-hidden")
         })

         it("hides the shopify box", () => {
            expect(document.getElementById("shopify").classList).toContain("is-hidden")
         })
      })
   })
})
