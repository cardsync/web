import { Application } from "stimulus"
import UserCardConditionIndexController from "packs/controllers/user_card_condition/index_controller"

const changeValue = (element, value, eventType): void => {
   const event = new Event(eventType)
   element.value = value
   element.dispatchEvent(event)
}

describe("UserCardConditionIndexController", () => {
   beforeEach(() => {
      document.body.innerHTML = `<form data-controller="index">
        <input id="quantity" type="number" value="4" data-target="index.quantity" data-action="keyup->index#quantityChange" />
        <input id="price" class="input" value="0.50" data-target="index.price" data-action="keyup->index#priceChange" type="text" />
        <input id="submit" type="submit" name="commit" value="Save" data-target="index.submit" />
      </form>`;

      const application = Application.start()
      application.register("index", UserCardConditionIndexController)
   })

   describe("when the quantity changes to less than zero", () => {
      beforeEach(() => {
         changeValue(document.getElementById("quantity"), "-1", "keyup")
      })

      it("disables submit button", () => {
         expect((document.getElementById("submit") as HTMLInputElement).disabled).toBe(true)
      })

      it("presents danger on the quantity input", () => {
         expect(document.getElementById("quantity").classList).toContain("is-danger")
      })

      describe("when the quantity changes back to positive", () => {
         beforeEach(() => {
            changeValue(document.getElementById("quantity"), "1", "keyup")
         });

         it("re-enables the submit button", () => {
            expect((document.getElementById("submit") as HTMLInputElement).disabled).toBe(false)
         })

         it("removes danger on the quantity input", () => {
            expect(document.getElementById("quantity").classList).not.toContain("is-danger")
         })
      })
   })
})
