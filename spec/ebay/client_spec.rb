require 'spec_helper'
require 'ebay/client'

describe Ebay::Client do
  let(:base_url) { 'base-url' }
  let(:current_token) { -> { 'current-token' } }
  let(:config) { {} }
  let(:http) { double(:http_rb) }

  let(:user_id) { 'user-id' }

  subject { described_class.new(base_url, current_token, config, http) }

  describe '#fetch_token' do
    let(:session_id) { 'a-session-id' }

    before :each do
      expect(http).to receive(:headers).and_return(http)
      expect(http).to receive(:post).with(
        "#{base_url}/ws/api.dll",
        body: %Q{<?xml version="1.0" encoding="utf-8"?>
        <FetchTokenRequest xmlns="urn:ebay:apis:eBLBaseComponents">
          <SessionID>#{session_id}</SessionID>
        </FetchTokenRequest>},
      ).and_return(%Q{
<FetchTokenResponse>
  <eBayAuthToken>some-token</eBayAuthToken>
  <HardExpirationTime>some-exp</HardExpirationTime>
</FetchTokenResponse>
})
    end

    it "returns the user's auth token" do
      expect(subject.fetch_token(session_id)[0]).to eq 'some-token'
    end

    it "returns the token exp time" do
      expect(subject.fetch_token(session_id)[1]).to eq 'some-exp'
    end
  end

  describe '#get_privilege' do
    let(:resp) { double(:resp, code: 200, body: '{"the":"body"}') }

    before :each do
      expect(http).to receive(:auth).with("Bearer #{current_token.call}").and_return(http)
      expect(http).to receive(:get).with("#{base_url}/sell/account/v1/privilege").and_return(resp)
    end

    it "returns the user's privilege" do
      expect(subject.get_privilege).to eq(the: 'body')
    end

    describe 'when ebay returns a 400' do
      let(:resp) { double(:resp, code: 400) }

      it 'raises an error' do
        expect { subject.get_privilege }.to raise_error(Ebay::Client::NotFound)
      end
    end

    describe 'when ebay returns a non 200' do
      let(:resp) { double(:resp, code: 500) }

      it 'raises an error' do
        expect { subject.get_privilege }.to raise_error(Ebay::Client::UnknownResponse)
      end
    end
  end
end
