require 'rails_helper'
require 'ebay/parse_item_transactions'

describe Ebay::ParseItemTransactions do
  let(:sale_xml) { file_fixture('ebay_sale.xml').read }

  before :each do
    @transaction_id, @item_id, @quantity_purchased = described_class.call(sale_xml)
  end

  it 'returns the transaction id' do
    expect(@transaction_id).to eq '1960934999011'
  end

  it 'returns the item id' do
    expect(@item_id).to eq '323450556740'
  end

  it 'returns the quantity purchased' do
    expect(@quantity_purchased).to eq 1
  end
end
