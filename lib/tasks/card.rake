require 'http'
require 'multi_json'

namespace :card do
  desc 'Save card collector number'
  task collector_number: :environment do
    Card.where(collector_number: nil).each do |c|
      jc = MultiJson.load(HTTP.get("https://api.scryfall.com/cards/#{c[:scryfall_id]}").to_s)
      c.collector_number = jc['collector_number']
      c.save!
    end
  end

  desc 'Update scryfall source image url'
  task sync_scryfall_image: :environment do
    CardSet.all.each do |set|
      url = "https://api.scryfall.com/cards/search?q=#{URI.escape("++e:#{set[:code]}")}"

      loop do
        cards = MultiJson.load(HTTP.get(url).to_s)

        cards['data'].each do |c|
          image_uris = if c['layout'] == 'transform'
                         c['card_faces'][0]['image_uris']
                       else
                         c['image_uris']
                       end

          if image_uris
            saved_card = Card.find_by_scryfall_id(c['id'])

            next unless saved_card
            next if saved_card.photo == image_uris['large']

            saved_card.photo = image_uris['large']

            saved_card.save!
          end
        end

        break unless cards['has_more']

        url = cards['next_page']
      end
    end
  end
end
