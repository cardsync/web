require 'http'
require 'multi_json'

namespace :cards do
  task price_history_today: :environment do |_, args|
    CardSet.all.each do |set|
      url = "https://api.scryfall.com/cards/search?format=json&include_extras=false&include_multilingual=false&order=set&page=1&q=e%3A#{set.code}&unique=prints"

      begin
        data = MultiJson.load(HTTP.get(url).to_s, symbolize_keys: true)
      rescue => e
        Rails.logger.debug(e.message)
        next
      end

      next unless data[:data]

      loop do
        data[:data].each do |c|
          card = Card.find_by_scryfall_id(c[:id])
          next unless card && c[:prices]

          prices = c[:prices]

          card.card_conditions.each do |cc|
            if ['near-mint', 'foil-near-mint'].include?(cc.condition) && CardConditionPriceHistory.where(card_condition_id: cc.id, date: Date.today).count == 0
              cc.card_condition_price_histories << CardConditionPriceHistory.new(
                date: Date.today,
                price: ((cc.condition == 'near-mint' ? prices[:usd].to_f : prices[:usd_foil].to_f) * 100).to_i,
              )
            end
          end
        end

        break unless data['has_more']

        url = data[:next_page]
      end
    end
  end

  task :set_price_history_today, [:code] => :environment do |_, args|
    url = "https://api.scryfall.com/cards/search?format=json&include_extras=false&include_multilingual=false&order=set&page=1&q=e%3A#{args[:code]}&unique=prints"

    data = MultiJson.load(HTTP.get(url).to_s, symbolize_keys: true)

    next unless data[:data]

    loop do
      data[:data].each do |c|
        card = Card.find_by_scryfall_id(c[:id])
        next unless card && c[:prices]

        prices = c[:prices]

        card.card_conditions.each do |cc|
          if ['near-mint', 'foil-near-mint'].include?(cc.condition) && CardConditionPriceHistory.where(card_condition_id: cc.id, date: Date.today).count == 0
            cc.card_condition_price_histories << CardConditionPriceHistory.new(
              date: Date.today,
              price: ((cc.condition == 'near-mint' ? prices[:usd].to_f : prices[:usd_foil].to_f) * 100).to_i,
            )
          end
        end
      end

      break unless data['has_more']

      url = data[:next_page]
    end
  end
end
