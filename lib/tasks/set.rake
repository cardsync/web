require 'http'
require 'multi_json'

namespace :set do
  desc 'Save set photos'
  task :photos, [:code] => :environment do |_, args|
    set = CardSet.find_by_code!(args[:code])

    set.cards.each do |c|
      next unless c.photo
      p "Uploading: #{c.card_set.id}/#{c.id}.jpg"

      c.remote_image_url = c.photo
      c.save!
    end
  end

  desc 'Save all set photos'
  task all_photos: :environment do |_, args|
    CardSet.all.each do |set|
      set.cards.each do |c|
        next unless c.photo
        p "Uploading: #{c.card_set.id}/#{c.id}.jpg"

        c.remote_image_url = c.photo
        c.save
      end
    end
  end

  desc "Add a set"
  task :add, [:code] => :environment do |_, args|
    set = MultiJson.load(HTTP.get("https://api.scryfall.com/sets/#{args[:code]}").to_s)

    saved_set = CardSet.find_by_code(args[:code]) || CardSet.create!(
      name: set['name'],
      code: set['code'],
      set_type: set['set_type'],
      released_at: set['released_at'],
    )

    url = "https://api.scryfall.com/cards/search?unique=prints&q=#{URI.escape("++e:#{args[:code]}")}"

    loop do
      cards = MultiJson.load(HTTP.get(url).to_s)

      cards['data'].each do |c|
        next if Card.find_by_scryfall_id(c['id'])

        saved_card = saved_set.cards.create!(
          scryfall_id: c['id'],
          name: c['name'],
          type_line: c['type_line'],
          rarity: c['rarity'],
          multiverseid: c['multiverse_ids'].first,
          number: c['collector_number'],
          names: nil,
          mana_cost: c['mana_cost'],
          cmc: c['cmc'],
          colors: c['colors'],
          color_identity: c['color_identity'],
          text: c['oracle_text'],
          flavor: c['flavor_text'],
          artist: c['artist'],
          power: c['power'],
          toughness: c['toughness'],
          loyalty: c['loyalty'],
          layout: c['layout'],
          reserved: c['reserved'],
          border: c['border_color'],
          photo: (c['image_uris']['large'] if c['image_uris']),
        )

        if c['nonfoil']
          ['near-mint', 'slightly-played', 'heavily-played'].each do |con|
            cc = saved_card.card_conditions.create!(
              condition: con,
            )

            User.all.each do |u|
              UserCardCondition.create!(
                user: u,
                card_condition: cc,
              )
            end
          end
        end

        if c['foil']
          ['foil-near-mint', 'foil-slightly-played', 'foil-heavily-played'].each do |con|
            cc = saved_card.card_conditions.create!(
              condition: con,
            )

            User.all.each do |u|
              UserCardCondition.create!(
                user: u,
                card_condition: cc,
              )
            end
          end
        end
      end

      break unless cards['has_more']

      url = cards['next_page']
    end
  end
end
