require 'multi_json'
require 'nikkou'
require 'ebay/error'

module Ebay
  class Client
    class NotFound < StandardError; end
    class UnknownResponse < StandardError; end

    class << self
      def auth_scopes
        [
          'https://api.ebay.com/oauth/api_scope',
          'https://api.ebay.com/oauth/api_scope/sell.inventory',
          'https://api.ebay.com/oauth/api_scope/sell.account',
        ].join(' ')
      end
    end

    def initialize(base_url, current_token, config, http)
      @base_url = base_url
      @current_token = current_token
      @config = config
      @http = http
    end

    def inventory_location(user_id)
      resp = @http.auth(
        "Bearer #{@current_token.call}",
      ).get(
        "#{@base_url}/sell/inventory/v1/location/#{user_id}",
      )

      if resp.code == 404
        raise NotFound
      end

      MultiJson.load(resp.body.to_s, symbolize_keys: true)
    end

    def create_location(user_id, address)
      resp = @http.auth(
        "Bearer #{@current_token.call}",
      ).headers(
        'Content-Type': 'application/json',
      ).post(
        "#{@base_url}/sell/inventory/v1/location/#{user_id}",
        body: MultiJson.dump({
          location: {
            address: {
              addressLine1: address.street,
              addressLine2: address.building,
              city: address.city,
              stateOrProvince: address.state_or_province,
              postalCode: address.postal_code,
              country: address.country,
            },
          },
          locationTypes: ["STORE"],
        })
      )

      body = MultiJson.load(resp.body.to_s, symbolize_keys: true)

      if resp.code != 204
        raise Error.new(body[:errors][0][:errorId], body[:errors][0][:message])
      end

      body
    end

    FulfillmentPolicy = Struct.new(
      :handling_time,
      :shipping_options,
      :currency,
    )

    FulfillmentPolicyShippingOption = Struct.new(
      :shipping_services,
    )

    FulfillmentPolicyShippingOptionShippingService = Struct.new(
      :additional_shipping_cost,
      :free_shipping,
      :shipping_cost,
    )

    def create_fulfillment_policy(user_id, policy)
      resp = @http.auth(
        "Bearer #{@current_token.call}",
      ).headers(
        'Content-Type': 'application/json',
      ).post(
        "#{@base_url}/sell/account/v1/fulfillment_policy",
        body: MultiJson.dump({
          name: user_id,
          marketplaceId: 'EBAY_US', # TODO: generalize this
          categoryTypes: [
            {
              default: true,
              name: 'ALL_EXCLUDING_MOTORS_VEHICLES',
            },
          ],
          globalShipping: false,
          handlingTime: {
            unit: 'DAY',
            value: policy.handling_time,
          },
          shippingOptions: policy.shipping_options.map { |so|
            {
              costType: 'FLAT_RATE',
              optionType: so.type,
              shippingServices: [
                {
                  buyerResponsibleForShipping: true,
                  shippingCarrierCode: 'USPS', #TODO this should be an option
                  shippingServiceCode: (
                    if so.type == :DOMESTIC
                      'USPSFirstClass'
                    elsif so.type == :INTERNATIONAL
                      'USPSFirstClassMailInternational'
                    end
                  ),
                  additionalShippingCost: {
                    currency: policy.currency,
                    value: so.additional_shipping_cost,
                  },
                  freeShipping: so.free_shipping,
                  shippingCost: {
                    currency: policy.currency,
                    value: so.shipping_cost,
                  },
                  shipToLocations: (
                    if so.type == :INTERNATIONAL
                      {
                        regionIncluded: [{
                          regionName: 'Worldwide',
                        }],
                      }
                    end
                  ),
                }
              ],
            }
          },
        })
      )

      body = MultiJson.load(resp.body.to_s, symbolize_keys: true)

      if resp.code != 201
        raise Error.new(body[:errors][0][:errorId], body[:errors][0][:message])
      end

      body
    end

    def update_fulfillment_policy(user_id, policy_id, policy)
      data = {
        name: user_id,
        marketplaceId: 'EBAY_US', # TODO: generalize this
        categoryTypes: [
          {
            default: true,
            name: 'ALL_EXCLUDING_MOTORS_VEHICLES',
          },
        ],
        globalShipping: false,
        handlingTime: {
          unit: 'DAY',
          value: policy.handling_time,
        },
        shippingOptions: policy.shipping_options.map { |so|
          {
            costType: 'FLAT_RATE',
            optionType: so.type,
            shippingServices: [
              {
                buyerResponsibleForShipping: true, # TODO: isn't this user supplied?
                shippingCarrierCode: 'USPS', #TODO this should be an option
                shippingServiceCode: (
                  if so.type == :DOMESTIC
                    'USPSFirstClass'
                  elsif so.type == :INTERNATIONAL
                    'USPSFirstClassMailInternational'
                  end
                ),
                additionalShippingCost: {
                  currency: policy.currency,
                  value: so.additional_shipping_cost,
                },
                freeShipping: so.free_shipping,
                shippingCost: {
                  currency: policy.currency,
                  value: so.shipping_cost,
                },
                shipToLocations: (
                  if so.type == :INTERNATIONAL
                    {
                      regionIncluded: [{
                        regionName: 'Worldwide',
                      }],
                    }
                  end
                ),
              }
            ],
          }
        },
      }

      resp = @http.auth(
        "Bearer #{@current_token.call}",
      ).headers(
        'Content-Type': 'application/json',
      ).put(
        "#{@base_url}/sell/account/v1/fulfillment_policy/#{policy_id}",
        body: MultiJson.dump(data)
      )

      if resp.code != 200
        raise 'omgggg'
      end
    end

    def get_fulfillment_policies
      resp = @http.auth(
        "Bearer #{@current_token.call}",
      ).get(
        # TODO: this shouldn't be hard coded to a marketplace id
        "#{@base_url}/sell/account/v1/fulfillment_policy?marketplace_id=EBAY_US",
      )

      if resp.code == 400
        raise NotFound
      end

      MultiJson.load(resp.body.to_s, symbolize_keys: true)
    end

    def get_payment_policies
      resp = @http.auth(
        "Bearer #{@current_token.call}",
      ).get(
        # TODO: this shouldn't be hard coded to a marketplace id
        "#{@base_url}/sell/account/v1/payment_policy?marketplace_id=EBAY_US",
      )

      if resp.code == 400
        raise NotFound
      elsif resp.code != 200
        raise UnknownResponse
      end

      MultiJson.load(resp.body.to_s, symbolize_keys: true)
    end

    def get_return_policies
      resp = @http.auth(
        "Bearer #{@current_token.call}",
      ).get(
        # TODO: this shouldn't be hard coded to a marketplace id
        "#{@base_url}/sell/account/v1/return_policy?marketplace_id=EBAY_US",
      )

      if resp.code == 400
        raise NotFound
      elsif resp.code != 200
        raise UnknownResponse
      end

      MultiJson.load(resp.body.to_s, symbolize_keys: true)
    end

    def get_privilege
      resp = @http.auth(
        "Bearer #{@current_token.call}",
      ).get(
        "#{@base_url}/sell/account/v1/privilege",
      )

      if resp.code == 400
        raise NotFound
      elsif resp.code != 200
        raise UnknownResponse
      end

      MultiJson.load(resp.body.to_s, symbolize_keys: true)
    end

    def set_notification_preferences(delivery_url, auth_token)
      @http.headers(
        'X-EBAY-API-CALL-NAME': 'SetNotificationPreferences',
        'X-EBAY-API-SITEID': '0',
        'X-EBAY-API-COMPATIBILITY-LEVEL': '1077',
      ).post(
        "#{@base_url}/ws/api.dll",
        body: %Q{<?xml version="1.0" encoding="utf-8"?>
        <SetNotificationPreferencesRequest xmlns="urn:ebay:apis:eBLBaseComponents">
          <RequesterCredentials>
            <eBayAuthToken>#{auth_token}</eBayAuthToken>
          </RequesterCredentials>
          <Version>1077</Version>
          <ApplicationDeliveryPreferences>
            <ApplicationEnable>Enable</ApplicationEnable>
            <ApplicationURL>#{delivery_url}</ApplicationURL>
            <DeviceType>Platform</DeviceType>
          </ApplicationDeliveryPreferences>
          <UserDeliveryPreferenceArray>
            <NotificationEnable>
              <EventType>FixedPriceTransaction</EventType>
              <EventEnable>Enable</EventEnable>
            </NotificationEnable>
          </UserDeliveryPreferenceArray>
        </SetNotificationPreferencesRequest>},
      )
    end

    def set_out_of_stock(auth_token)
      @http.headers(
        'X-EBAY-API-CALL-NAME': 'SetUserPreferencesRequest',
        'X-EBAY-API-SITEID': '0',
        'X-EBAY-API-COMPATIBILITY-LEVEL': '1077',
      ).post(
        "#{@base_url}/ws/api.dll",
        body: %Q{<?xml version="1.0" encoding="utf-8"?>
        <SetUserPreferencesRequest xmlns="urn:ebay:apis:eBLBaseComponents">
          <RequesterCredentials>
            <eBayAuthToken>#{auth_token}</eBayAuthToken>
          </RequesterCredentials>
          <Version>1077</Version>
          <OutOfStockControlPreference>true</OutOfStockControlPreference>
        </SetUserPreferencesRequest>},
      )
    end

    def get_notification_preferences(level)
      @http.headers(
        'X-EBAY-API-IAF-TOKEN': @current_token.call,
        'X-EBAY-API-CALL-NAME': 'GetNotificationPreferences',
        'X-EBAY-API-SITEID': '0',
        'X-EBAY-API-COMPATIBILITY-LEVEL': '1077',
      ).post(
        "#{@base_url}/ws/api.dll",
        body: %Q{<?xml version="1.0" encoding="utf-8"?>
        <GetNotificationPreferencesRequest xmlns="urn:ebay:apis:eBLBaseComponents">
          <PreferenceLevel>#{level}</PreferenceLevel>
        </GetNotificationPreferencesRequest>},
        ).to_s
    end

    def get_notifications_usage(item_id)
      @http.headers(
        'X-EBAY-API-IAF-TOKEN': @current_token.call,
        'X-EBAY-API-CALL-NAME': 'GetNotificationsUsage',
        'X-EBAY-API-SITEID': '0',
        'X-EBAY-API-COMPATIBILITY-LEVEL': '1077',
      ).post(
        "#{@base_url}/ws/api.dll",
        body: %Q{<?xml version="1.0" encoding="utf-8"?>
        <GetNotificationsUsageRequest xmlns="urn:ebay:apis:eBLBaseComponents">
          <ItemID>#{item_id}</ItemID>
          <StartTime>#{(Time.now - 43200).utc}</StartTime>
        </GetNotificationsUsageRequest>},
        ).to_s
    end

    def get_item_transactions(item_id)
      @http.headers(
        'X-EBAY-API-IAF-TOKEN': @current_token.call,
        'X-EBAY-API-CALL-NAME': 'GetItemTransactions',
        'X-EBAY-API-SITEID': '0',
        'X-EBAY-API-COMPATIBILITY-LEVEL': '1077',
      ).post(
        "#{@base_url}/ws/api.dll",
        body: %Q{<?xml version="1.0" encoding="utf-8"?>
        <GetItemTransactionsRequest xmlns="urn:ebay:apis:eBLBaseComponents">
          <ItemID>#{item_id}</ItemID>
          <DetailLevel>ReturnAll</DetailLevel>
        </GetItemTransactionsRequest>},
        ).to_s
    end

    def get_ebay_details(detail_name)
      resp = @http.headers(
        'X-EBAY-API-IAF-TOKEN': @current_token.call,
        'X-EBAY-API-CALL-NAME': 'GetEbayDetails',
        'X-EBAY-API-SITEID': '0',
        'X-EBAY-API-COMPATIBILITY-LEVEL': '1077',
      ).post(
        "#{@base_url}/ws/api.dll",
        body: %Q{<?xml version="1.0" encoding="utf-8"?>
        <GeteBayDetailsRequest xmlns="urn:ebay:apis:eBLBaseComponents">
          <Version>1077</Version>
          <DetailName>#{detail_name}</DetailName>
        </GeteBayDetailsRequest>},
      )

      Nokogiri::XML(resp.to_s).remove_namespaces!
    end

    def get_session_id
      resp = @http.headers(
        'X-EBAY-API-CALL-NAME': 'GetSessionID',
        'X-EBAY-API-APP-NAME': @config['app_id'],
        'X-EBAY-API-DEV-NAME': @config['dev_id'],
        'X-EBAY-API-CERT-NAME': @config['client_secret'],
        'X-EBAY-API-SITEID': '0',
        'X-EBAY-API-COMPATIBILITY-LEVEL': '1077',
      ).post(
        "#{@base_url}/ws/api.dll",
        body: %Q{<?xml version="1.0" encoding="utf-8"?>
        <GetSessionIDRequest xmlns="urn:ebay:apis:eBLBaseComponents">
          <RuName>#{@config['ru_name']}</RuName>
        </GetSessionIDRequest>},
      )

      ebay_resp = Nokogiri::XML(resp.to_s)
      ebay_resp.remove_namespaces!

      ebay_resp.xpath('//GetSessionIDResponse/SessionID').first.content
    end

    def fetch_token(session_id)
      resp = @http.headers(
        'X-EBAY-API-CALL-NAME': 'FetchToken',
        'X-EBAY-API-APP-NAME': @config['app_id'],
        'X-EBAY-API-DEV-NAME': @config['dev_id'],
        'X-EBAY-API-CERT-NAME': @config['client_secret'],
        'X-EBAY-API-SITEID': '0',
        'X-EBAY-API-COMPATIBILITY-LEVEL': '1077',
      ).post(
        "#{@base_url}/ws/api.dll",
        body: %Q{<?xml version="1.0" encoding="utf-8"?>
        <FetchTokenRequest xmlns="urn:ebay:apis:eBLBaseComponents">
          <SessionID>#{session_id}</SessionID>
        </FetchTokenRequest>},
      )

      ebay_resp = Nokogiri::XML(resp.to_s)
      ebay_resp.remove_namespaces!

      token = ebay_resp.xpath('//FetchTokenResponse/eBayAuthToken').first.content
      expires_at = ebay_resp.xpath('//FetchTokenResponse/HardExpirationTime').first.content

      [token, expires_at]
    end
  end
end
