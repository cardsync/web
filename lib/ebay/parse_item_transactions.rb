require 'nokogiri'

module Ebay
  class ParseItemTransactions
    class << self
      def call(sale_xml)
        sale = Nokogiri::XML(sale_xml)
        sale.remove_namespaces!
        item_transaction_response = sale.xpath('//Envelope//Body//GetItemTransactionsResponse')

        return unless item_transaction_response.first

        listing_type = item_transaction_response.xpath('//ListingType').first.content

        transaction_id = item_transaction_response.xpath('//TransactionID').first.content

        item = item_transaction_response.xpath('//Item').first
        item_id = item.xpath('//ItemID').first.content

        transaction = item_transaction_response.xpath('//TransactionArray//Transaction').first
        quantity_purchased = transaction.xpath('//QuantityPurchased').first.content.to_i

        [transaction_id, item_id, quantity_purchased]
      end
    end
  end
end
