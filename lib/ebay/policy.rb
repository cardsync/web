module Ebay
  Policy = Struct.new(:handling_time, :currency, :shipping_options)
  ShippingPolicy = Struct.new(:type, :free_shipping, :shipping_cost, :additional_shipping_cost)
end
