module Ebay
  module Decorators
    Fulfillment = Struct.new(
      :handling_time,
      :shipping_options,
    ) do
      def currency
        return nil unless shipping_options

        shipping_options.first[:shippingServices].first[:shippingCost][:currency]
      end

      def shipping_cost
        return nil unless shipping_options

        so = shipping_options.find { |so|
          so[:optionType] == 'DOMESTIC'
        }

        return nil unless so

        so[:shippingServices].first[:shippingCost][:value].to_i
      end

      def additional_shipping_cost
        return nil unless shipping_options

        so = shipping_options.find { |so|
          so[:optionType] == 'DOMESTIC'
        }

        return nil unless so

        so[:shippingServices].first[:additionalShippingCost][:value].to_i
      end

      def international?
        return nil unless shipping_options

        !shipping_options.find { |so|
          so[:optionType] == 'INTERNATIONAL'
        }.nil?
      end

      def international_shipping_cost
        return nil unless shipping_options

        so = shipping_options.find { |so|
          so[:optionType] == 'INTERNATIONAL'
        }

        return nil unless so

        so[:shippingServices].first[:shippingCost][:value].to_i
      end

      def international_additional_shipping_cost
        return nil unless shipping_options

        so = shipping_options.find { |so|
          so[:optionType] == 'INTERNATIONAL'
        }

        return nil unless so

        so[:shippingServices].first[:additionalShippingCost][:value].to_i
      end
    end
  end
end
