module Secured
  extend ActiveSupport::Concern

  included do
    before_action :logged_in_using_omniauth?
  end

  def logged_in_using_omniauth?
    redirect_to '/' unless session[:userinfo].present? && current_user
  end

  def current_user
    @current_user ||= User.find_by_uid(session[:userinfo]['uid'])
  end
end
