require 'ebay/client'
require 'http'

module MarketplaceData
  extend ActiveSupport::Concern
  include Secured

  included do
    before_action :load_ebay_client
    before_action :set_ebay_data, except: [:update, :create]
    before_action :set_shopify_data, except: [:update, :create]
  end

  def load_ebay_client
    return unless current_user.user_ebay

    @ebay_client = Ebay::Client.new(
      Rails.configuration.ebay['base_url'],
      current_user.user_ebay.method(:current_token),
      Rails.configuration.ebay,
      HTTP.use(logging: {logger: Rails.logger}),
    )

    @has_ebay = current_user.valid_ebay_token
    @has_ebay_legacy_token = current_user.user_ebay && !!current_user.user_ebay.legacy_auth_token
  end

  def load_fulfillment_policy
    @has_ebay_fulfillment_policy = !!current_user.user_ebay && !!current_user.user_ebay.fulfillment_policy_id
  end

  def load_location
    return unless current_user.user_ebay

    @ebay_location =
      begin
        @ebay_client.inventory_location(current_user.id)
      rescue Ebay::Client::NotFound
      end
  end

  def load_payment_policy
    @has_ebay_payment_policy = current_user.user_ebay && !!current_user.user_ebay.payment_policy_id
  end

  def load_return_policy
    @has_ebay_return_policy = current_user.user_ebay && !!current_user.user_ebay.return_policy_id
  end

  def set_ebay_data
    return unless current_user.user_ebay

    load_location
    load_fulfillment_policy
    load_payment_policy
    load_return_policy

    @template = current_user.user_ebay.template
  end

  def set_shopify_data
    @has_shopify = !!current_user.user_shopify
  end
end
