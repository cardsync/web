class StaticController < ApplicationController
  def privacy; end
  def terms; end
  def product; end
  def pricing; end

  def not_found; end
  def internal_server_error; end
end
