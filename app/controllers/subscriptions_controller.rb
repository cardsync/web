class SubscriptionsController < ApplicationController
  include Secured

  def show
    @card = Stripe::Customer.retrieve(current_user.stripe_customer_id).sources.data.first
    @discount = current_user.stripe_subscription.discount.coupon.name if current_user.stripe_subscription.discount
  end

  def update
    customer = Stripe::Customer.retrieve(current_user.stripe_customer_id)

    if params[:card_token] && params[:card_token] != ''
      customer.source = params[:card_token]
      customer.save
    end

    sub = current_user.stripe_subscription
    sub.coupon = params[:promo_code] if params[:promo_code] && params[:promo_code] != ''
    sub.items = [{
      id: sub.items.data[0].id,
      plan: params[:plan],
    }]
    sub.save

    redirect_to subscription_path
  end
end
