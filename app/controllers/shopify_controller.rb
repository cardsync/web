class ShopifyController < ActionController::Base 
  skip_before_action :verify_authenticity_token

  def notification
    unless request.env['HTTP_X_SHOPIFY_SHOP_DOMAIN'] && request.env['HTTP_X_SHOPIFY_HMAC_SHA256']
      head :bad_request
      return
    end

    shop = UserShopify.find_by_shop!(request.env['HTTP_X_SHOPIFY_SHOP_DOMAIN'][/(.+).myshopify.com/, 1])
    body = request.raw_post

    calculated_hmac = Base64.strict_encode64(OpenSSL::HMAC.digest('sha256', shop.shared_secret, body))

    unless ActiveSupport::SecurityUtils.secure_compare(calculated_hmac, request.env['HTTP_X_SHOPIFY_HMAC_SHA256'])
      head :bad_request
      return
    end

    MultiJson.load(body, symbolize_keys: true)[:line_items].each do |line_item|
      succ = ShopifyUserCardConditions.find_by_shopify_product_variant_id(line_item[:variant_id])
      if succ
        ucc = succ.user_card_condition
        ucc.quantity -= line_item[:quantity]
        ucc.save!
      end
    end

    head :ok
  end

  private
end
