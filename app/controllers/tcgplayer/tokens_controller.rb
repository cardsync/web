require 'http'

module Tcgplayer
  class TokensController < ApplicationController
    include Secured
    include MarketplaceData

    before_action :load_has_tcgplayer

    def show; end

    def create
      resp = HTTP.use(logging: {logger: Rails.logger}).post("https://api.tcgplayer.com/app/authorize/#{params[:code]}")

      body = MultiJson.load(resp.body.to_s, symbolize_keys: true)

      # TODO: show error message
      unless body[:success]
        redirect_to tcgplayer_token_path 
        return
      end

      UserTcgplayer.create!(
        user: current_user,
        access_token: body[:AuthorizationKey],
      )

      redirect_to tcgplayer_token_path
    end

    protected

    def load_has_tcgplayer
      @has_tcgplayer = !!current_user.user_tcgplayer
    end
  end
end
