require 'ebay/parse_item_transactions'

class EbayController < ActionController::Base 
  skip_before_action :verify_authenticity_token

  def notification
    transaction_id, item_id, quantity_purchased = Ebay::ParseItemTransactions.call(request.raw_post)

    # FIXME: verify post is from ebay: https://developer.ebay.com/DevZone/guides/ebayfeatures/Notifications/Notifications.html#SOAPMessageHeaderNotificationSignature

    if transaction_id && item_id && quantity_purchased
      eo = EbayOffer.find_by_listing_id(item_id)
      if eo
        ucc = eo.user_card_condition
        ucc.quantity -= quantity_purchased * eo.quantity
        ucc.save!

        EbayTransactionHistory.create!(
          user_card_condition: ucc,
          quantity: quantity_purchased,
          transaction_id: transaction_id,
          xml: request.raw_post,
        )
      end
    end

    head :ok
  end
end
