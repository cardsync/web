class AddressesController < ApplicationController
  def show
    @address = current_user.user_address || UserAddress.new
  end

  def update
    if current_user.user_address
      current_user.user_address.update(input_params)
    else
      current_user.create_user_address(input_params)
    end

    if current_user.user_address.valid?
      redirect_to address_path 
      return
    end

    @address = current_user.user_address
    render :show
  end

  private

  def input_params
    params.require(:user_address).permit(:street, :building, :city, :subnational, :postal_code, :country)
  end
end
