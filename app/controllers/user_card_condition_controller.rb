require 'ebay/client'
require 'http'

class UserCardConditionController < ApplicationController
  include Secured

  before_action :load_ebay_client

  content_security_policy only: :show do |policy|
    policy.script_src :self, :https, :unsafe_inline
  end

  def index
    q = current_user.
      user_card_conditions.
      includes(card_condition: [{card: :card_set}]).
      joins(card_condition: [{card: :card_set}]).
      order("card_sets.released_at desc, regexp_replace(cards.number, '\D', '', 'g')").
      page(params[:page] || 1)

    if !params[:card] || params[:card][:in_stock]
      q = q.where('user_card_conditions.quantity > 0')
    end

    if params[:card] && params[:card][:name] && !params[:card][:name].empty?
      q = q.where('cards.name ~* ?', Regexp.escape(params[:card][:name]))
    end

    if params[:card] && params[:card][:condition] && !params[:card][:condition].empty? && params[:card][:condition] != 'Condition'
      q = q.where('card_conditions.condition = ?', params[:card][:condition])
    end

    if params[:card] && params[:card][:rarity] && !params[:card][:rarity].empty? && params[:card][:rarity] != 'Rarity'
      q = q.where('cards.rarity = ?', params[:card][:rarity])
    end

    if params[:card] && params[:card][:set_id] && params[:card][:set_id] != 'Set'
      q = q.where('card_sets.id = ?', params[:card][:set_id])
    end

    @user_card_conditions = q.all

    @card_sets = CardSet.where.not(released_at: nil).order(released_at: :desc).all + 
      CardSet.where(released_at: nil).order(name: :asc).all

    @conditions = [
      'near-mint',
      'slightly-played',
      'heavily-played',
      'foil-near-mint',
      'foil-slightly-played',
      'foil-heavily-played',
    ]

    @rarities = [
      'common',
      'uncommon',
      'rare',
      'mythic',
    ]
  end

  def show
    @user_card_condition = current_user.user_card_conditions.includes(card_condition: [{card: :card_set}]).find_by_id(params[:id])

    unless @user_card_condition
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  def update
    current_user.
      user_card_conditions.
      where(id: params[:id]).
      update(quantity: params[:user_card_condition][:quantity], price: (params[:user_card_condition][:price].to_f * 100).to_i)
  end

  private

  def load_ebay_client
    return unless current_user.user_ebay

    @ebay_client = Ebay::Client.new(
      Rails.configuration.ebay['base_url'],
      current_user.user_ebay.method(:current_token),
      Rails.configuration.ebay,
      HTTP.use(logging: {logger: Rails.logger}),
    )

    @has_ebay = current_user.valid_ebay_token
  end
end
