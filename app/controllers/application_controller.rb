class ApplicationController < ActionController::Base
  rescue_from ActionController::UnknownFormat do
    redirect_to internal_server_error_path
  end

  def user_signed_in?
    !!session[:userinfo]
  end
end
