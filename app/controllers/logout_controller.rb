class LogoutController < ApplicationController
  def logout
    reset_session
    redirect_to logout_url.to_s
  end

  private

  def logout_url
    domain = 'synkt.auth0.com'
    client_id = ENV['AUTH0_CLIENT_ID']

    request_params = {
      returnTo: root_url,
      client_id: client_id
    }

    URI::HTTPS.build(host: domain, path: '/v2/logout', query: to_query(request_params))
  end

  def to_query(hash)
    hash.map { |k, v| "#{k}=#{URI.escape(v)}" unless v.nil? }.reject(&:nil?).join('&')
  end
end
