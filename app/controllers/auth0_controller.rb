class Auth0Controller < ApplicationController
  def callback
    redirect_to root_path unless request.env['omniauth.auth']

    auth = request.env['omniauth.auth']

    session[:userinfo] = {
      'provider': auth[:provider],
      'uid': auth[:uid],
      'info': {
        'name': auth[:info][:name],
        'email': auth[:info][:email],
        'first_name': auth[:info][:first_name],
        'last_name': auth[:info][:last_name],
      },
    }

    User.find_or_create_from_auth_hash(session[:userinfo])

    redirect_to user_card_condition_index_path
  end

  def failure
    # show a failure page or redirect to an error page
    @error_msg = request.params['message']
  end
end
