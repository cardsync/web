class HomeController < ApplicationController
  def index
    redirect_to user_card_condition_index_path if user_signed_in?
  end
end
