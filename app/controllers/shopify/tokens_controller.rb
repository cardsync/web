require 'http'

module Shopify
  class TokensController < ApplicationController
    include Secured
    include MarketplaceData

    def show
      pp @has_shopify, current_user.user_shopify
    end

    def create
      resp = HTTP.use(logging: {logger: Rails.logger}).basic_auth(:user => params[:api_key], :pass => params[:password]).get("https://#{params[:shop]}.myshopify.com/admin/api/2020-04/shop.json")

      # TODO: show error message
      unless resp.code == 200
        redirect_to shopify_token_path 
        return
      end

      UserShopify.create!(
        user: current_user,
        shop: params[:shop],
        api_key: params[:api_key],
        password: params[:password],
        shared_secret: params[:shared_secret],
      )

      HTTP.basic_auth(
        :user => params[:api_key], :pass => params[:password],
      ).headers(
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      ).post(
        "https://#{params[:shop]}.myshopify.com/admin/api/2020-04/webhooks.json",
        body: MultiJson.dump({
          webhook: {
            topic: 'orders/create',
            address: "https://web-uaexzjfedq-uc.a.run.app#{shopify_notification_path}",
            format: 'json',
          },
        })
      )

      redirect_to shopify_token_path
    end
  end
end
