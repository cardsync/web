require 'ebay/error'
require 'ebay/policy'
require 'ebay/decorators/fulfillment'

module Ebay
  class FulfillmentsController < ApplicationController
    include MarketplaceData

    def edit
      @policies = @ebay_client.get_fulfillment_policies[:fulfillmentPolicies].map { |p| [p[:name], p[:fulfillmentPolicyId]] }
      @policy_id = current_user.user_ebay.fulfillment_policy_id
    end

    def update
      current_user.user_ebay.update!(
        fulfillment_policy_id: params[:policy_id],
      )
    end
  end
end
