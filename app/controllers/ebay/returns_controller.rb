module Ebay
  class ReturnsController < ApplicationController
    include MarketplaceData

    def edit
      @policies = @ebay_client.get_return_policies[:returnPolicies].map { |p| [p[:name], p[:returnPolicyId]] }
      @policy_id = current_user.user_ebay.return_policy_id
    end

    def update
      current_user.user_ebay.update!(
        return_policy_id: params[:policy_id],
      )
    end
  end
end
