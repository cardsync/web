require 'ebay/client'
require 'http'

module Ebay
  class LocationsController < ApplicationController
    include MarketplaceData

    before_action :load_location, only: [:update]

    def edit
      location = Struct.new(
        :street,
        :building,
        :city,
        :state_or_province,
        :postal_code,
        :country,
      )

      @inventory_location = if @ebay_location
        location.new(
          @ebay_location[:location][:address][:addressLine1],
          @ebay_location[:location][:address][:addressLine2],
          @ebay_location[:location][:address][:city],
          @ebay_location[:location][:address][:stateOrProvince],
          @ebay_location[:location][:address][:postalCode],
          'US',
        )
      else
        location.new
      end
    end

    def update
      Ebay::Client.new(
        Rails.configuration.ebay['base_url'],
        current_user.user_ebay.method(:current_token),
        Rails.configuration.ebay,
        HTTP.use(logging: {logger: Rails.logger}),
      ).create_location(
        current_user.id,
        Struct.new(
          :street,
          :building,
          :city,
          :state_or_province,
          :postal_code,
          :country,
        ).new(
          params[:street],
          params[:building],
          params[:city],
          params[:state_or_province],
          params[:postal_code],
          'US',
        ),
      )
    rescue Ebay::Error => e
      render :json => MultiJson.dump(
        code: e.code,
        message: e.message,
      ), status: 400
    end
  end
end
