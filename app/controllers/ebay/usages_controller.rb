module Ebay
  class UsagesController < ApplicationController
    include MarketplaceData

    def show
      p = @ebay_client.get_privilege
      @ebay_selling_stats = if p[:sellingLimit]
                              {
                                value: p[:sellingLimit][:amount][:value].to_i,
                                max: p[:sellingLimit][:quantity].to_i,
                              }
                            else
                              {
                                value: 'Unknown',
                                max: 'Unknown',
                              }
                            end
    end
  end
end
