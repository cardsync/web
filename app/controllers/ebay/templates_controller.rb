module Ebay
  class TemplatesController < ApplicationController
    include MarketplaceData

    def edit
    end

    def update
      if params[:template] && !params[:template].empty?
        current_user.user_ebay.update(template: params[:template])
      end
    end
  end
end
