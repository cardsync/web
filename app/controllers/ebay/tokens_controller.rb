require 'http'

module Ebay
  class TokensController < ApplicationController
    include MarketplaceData

    def show; end

    def request_grant
      redirect_to "#{Rails.configuration.ebay['auth_url']}/oauth2/authorize?"+
        "client_id=#{Rails.configuration.ebay['client_id']}&"+
        "response_type=code&"+
        "redirect_uri=#{Rails.configuration.ebay['ru_name']}&"+
        "scope=#{URI.escape(Ebay::Client.auth_scopes)}"
    end

    def start_fetch_legacy_token
      session_id = @ebay_client.get_session_id

      session[:ebay_session_id] = session_id

      redirect_to "https://signin.ebay.com/ws/eBayISAPI.dll?SignIn&"+
        "runame=#{Rails.configuration.ebay['ru_name']}"+
        "&SessID=#{session_id}"
    end

    def save_token
      if params[:code] # oauth2
        resp = HTTP.use(logging: {logger: Rails.logger}).basic_auth(
          user: Rails.configuration.ebay['client_id'],
          pass: Rails.configuration.ebay['client_secret'],
        ).post(
          "#{Rails.configuration.ebay['base_url']}/identity/v1/oauth2/token",
          form: {
            grant_type: :authorization_code,
            redirect_uri: Rails.configuration.ebay['ru_name'],
            code: params[:code],
          },
        )

        if resp.code == 200
          body = MultiJson.load(resp.to_s)

          current_user.user_ebay = UserEbay.create!(
            user_id: current_user.id,
            auth_token: body['access_token'],
            auth_token_expires_at: Time.now + body['expires_in'],
            refresh_token: body['refresh_token'],
            refresh_token_expires_at: Time.now + body['refresh_token_expires_in'],
          )

          redirect_to ebay_token_path
        end
      else # auth n auth
        token, expires_at = @ebay_client.fetch_token(session[:ebay_session_id])

        current_user.user_ebay.update!(
          legacy_auth_token: token,
          legacy_auth_token_expires_at: expires_at,
        )

        Ebay::Client.new(
          Rails.configuration.ebay['base_url'],
          current_user.user_ebay.method(:current_token),
          Rails.configuration.ebay,
          HTTP.use(logging: {logger: Rails.logger}),
        ).set_notification_preferences("https://web-uaexzjfedq-uc.a.run.app#{ebay_notification_path}", token)

        Ebay::Client.new(
          Rails.configuration.ebay['base_url'],
          current_user.user_ebay.method(:current_token),
          Rails.configuration.ebay,
          HTTP.use(logging: {logger: Rails.logger}),
        ).set_out_of_stock(token)

        redirect_to ebay_notification_path
      end
    end
  end
end
