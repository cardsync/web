require 'ebay/error'

module Ebay
  class PaymentsController < ApplicationController
    include MarketplaceData

    def edit
      @policies = @ebay_client.get_payment_policies[:paymentPolicies].map { |p| [p[:name], p[:paymentPolicyId]] }
      @policy_id = current_user.user_ebay.payment_policy_id
    end

    def update
      current_user.user_ebay.update!(
        payment_policy_id: params[:policy_id],
      )
    end
  end
end
