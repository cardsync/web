class User < ApplicationRecord
  has_many :user_card_conditions
  has_many :card_conditions, through: :user_card_conditions
  has_one :user_ebay
  has_one :user_tcgplayer
  has_one :user_shopify
  has_one :user_address

  attr_accessor :sign_up_code

  before_create :create_stripe_customer
  after_create :populate_cards

  def self.find_or_create_from_auth_hash(auth_hash)
    user = User.find_by_uid(auth_hash[:uid])

    return user if user

    User.create!(
      uid: auth_hash[:uid],
      email: auth_hash[:info][:email],
      first_name: auth_hash[:info][:first_name],
      last_name: auth_hash[:info][:last_name],
    )
  end

  def valid_ebay_token
    self.user_ebay && self.user_ebay.refresh_token_expires_at > Time.now
  end

  def stripe_subscription
    @sub ||= Stripe::Subscription.retrieve(self.stripe_subscription_id)
  end

  def stripe_subscription_plan
    @plan ||= self.stripe_subscription.plan
  end

  def free_plan?
    self.stripe_subscription_plan.id == Rails.configuration.stripe['free_subscription']
  end

  def basic_plan?
    self.stripe_subscription_plan.id == Rails.configuration.stripe['basic_subscription']
  end

  def stripe_discount_name
    self.stripe_subscription.discount['coupon']&.name
  end

  private

  def create_stripe_customer
    self.stripe_customer_id = Stripe::Customer.create(
      email: self.email,
    ).id

    self.stripe_subscription_id = Stripe::Subscription.create(
      customer: self.stripe_customer_id,
      items: [
        { plan: Rails.configuration.stripe['free_subscription'] },
      ],
    ).id
  end

  def populate_cards
    # TODO: put this into a pubsub to call function
    ActiveRecord::Base.connection.execute(
      "insert into user_card_conditions (
        user_id, card_condition_id, created_at, updated_at
      ) SELECT '#{self.id}', id, now(), now() FROM card_conditions",
    )
  end
end
