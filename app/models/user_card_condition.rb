require 'google/cloud/pubsub'

class UserCardCondition < ApplicationRecord
  belongs_to :user
  belongs_to :card_condition
  has_many :user_card_condition_ebay_api_histories
  has_many :user_card_condition_shopify_api_histories
  has_many :ebay_offers

  has_many :card_condition_price_histories, through: :card_condition

  after_commit :send_change_notification, if: -> { has_ebay_token? || has_shopify_token? }

  private

  def send_change_notification
    Google::Cloud::PubSub.new(
      project_id: 'synkt-1556838469966',
    ).topic('inventory-changed').publish(
      MultiJson.dump(
        user_card_condition_id: self.id,
        user_id: self.user.id,
        quantity: self.quantity,
        price: self.price,
        condition: self.card_condition.condition,
        card_name: self.card_condition.card.name,
        set_name: self.card_condition.card.card_set.name,
      )
    )
  end

  def has_ebay_token?
    self.user.user_ebay && self.user.user_ebay.auth_token && self.user.user_ebay.legacy_auth_token
  end

  def has_shopify_token?
    !!self.user.user_shopify
  end
end
