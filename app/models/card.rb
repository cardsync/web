class Card < ApplicationRecord
  belongs_to :card_set
  has_many :card_conditions

  def image_url
    "https://storage.cloud.google.com/synkt-card-images/#{card_set.id}/#{id}.jpg"
  end
end
