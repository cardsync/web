class CardCondition < ApplicationRecord
  belongs_to :card
  has_many :card_condition_price_histories

  def latest_price
    h = self.card_condition_price_histories.where('price > 0').order(date: :desc).first

    '$%.2f' % (h.price / 100.0) if h
  end
end
