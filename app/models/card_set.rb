class CardSet < ApplicationRecord
  validates :code, uniqueness: true
  has_many :cards
end
