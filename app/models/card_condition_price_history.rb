class CardConditionPriceHistory < ApplicationRecord
  belongs_to :card_condition

  scope :latest, -> { order(:date).limit(1) }
end
