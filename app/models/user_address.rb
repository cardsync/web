class UserAddress < ApplicationRecord
  belongs_to :user

  validates :street, :city, :subnational, :postal_code, :country, presence: true
end
