class UserEbay < ApplicationRecord
  belongs_to :user

  def current_token
    if self.auth_token_expires_at - Time.now < 10.seconds
      Rails.logger.debug('Refreshing auth token')

      resp = HTTP.basic_auth(
        user: Rails.configuration.ebay['client_id'],
        pass: Rails.configuration.ebay['client_secret'],
      ).post(
        "#{Rails.configuration.ebay['base_url']}/identity/v1/oauth2/token",
        form: {
          grant_type: :refresh_token,
          refresh_token: self.refresh_token,
          scope: Ebay::Client.auth_scopes,
        },
      )

      if resp.code == 200
        body = MultiJson.load(resp.to_s)

        self.auth_token = body['access_token']
        self.auth_token_expires_at = Time.now + body['expires_in']

        self.save
      else
        Rails.logger.debug('Unable to refresh token')
        Rails.logger.debug(resp.code)
        Rails.logger.debug(resp.to_s)
      end
    end

    self.auth_token
  end
end
