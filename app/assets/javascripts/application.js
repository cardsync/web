// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks

function initFreshChat() {
   window.fcWidget.init({
      token: "e3113812-3a8a-4596-a574-89dcf75f1e37",
      host: "https://wchat.freshchat.com"
   });

   window.fcWidget.setExternalId(document.querySelector('meta[name="user_id"]').getAttribute("content"));
   window.fcWidget.user.setEmail(document.querySelector('meta[name="user_email"]').getAttribute("content"));
   window.fcWidget.user.setFirstName(document.querySelector('meta[name="user_first_name"]').getAttribute("content"));
}
function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
