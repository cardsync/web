import $ from 'cash-dom'

document.addEventListener('ajax:success', () => {
  location.reload()
})

document.addEventListener('ajax:before', () => {
  $('.notification.is-danger').addClass('is-hidden')
})

document.addEventListener('ajax:error', (event) => {
  $('.notification.is-danger').text(`[${event.detail[0].code}] An error occurred saving: ${event.detail[0].message}`)
  $('.notification.is-danger').removeClass('is-hidden')
})
