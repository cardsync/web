import { Controller } from "stimulus"

class IndexBaseController extends Controller {
   public quantityTarget!: HTMLInputElement;
   public priceTarget!: HTMLInputElement;
   public submitTarget!: HTMLInputElement;
}

export default class UserCardConditionIndexController extends (Controller as typeof IndexBaseController) {
   static targets = [
      "quantity", "price", "submit"
   ]

   quantityChange(): void {
      if (this.quantityTarget.value == "" || Number(this.quantityTarget.value) < 0) {
         this.quantityTarget.classList.add("is-danger")
      } else {
         this.quantityTarget.classList.remove("is-danger")
      }

      this.validateForm()
   }

   priceChange(): void {
      if (this.price() < 0 || isNaN(this.price())) {
         this.priceTarget.classList.add("is-danger")
      } else {
         this.priceTarget.classList.remove("is-danger")
      }

      this.validateForm()
   }

   validateForm(): void {
      this.submitTarget.disabled = this.validForm()
   }

   private validForm(): boolean {
      return this.quantityTarget.value === "" ||
         Number(this.quantityTarget.value) < 0 ||
         this.price() < 0 ||
         isNaN(this.price())
   }

   private price(): number {
      return Number.parseFloat(this.priceTarget.value)
   }
}
