import { Controller } from "stimulus"

class ShowBaseController extends Controller {
   public ebayTarget!: HTMLInputElement;
   public shopifyTarget!: HTMLInputElement;

   public ebayButtonTarget!: HTMLInputElement;
   public shopifyButtonTarget!: HTMLInputElement;
}

export default class extends (Controller as typeof ShowBaseController) {
   static targets: string[] = [ "ebay", "shopify", "ebayButton", "shopifyButton" ]

	connect(): void {
		// console.log("hello show!")
	}

   showEbay(): void {
      this.ebayTarget.classList.remove("is-hidden")
      this.ebayButtonTarget.classList.add("is-active")
      this.shopifyTarget.classList.add("is-hidden")
      this.shopifyButtonTarget.classList.remove("is-active")
   }

   showShopify(): void {
      this.ebayTarget.classList.add("is-hidden")
      this.ebayButtonTarget.classList.remove("is-active")
      this.shopifyTarget.classList.remove("is-hidden")
      this.shopifyButtonTarget.classList.add("is-active")
   }
}
