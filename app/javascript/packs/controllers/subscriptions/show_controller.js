import { Controller } from 'stimulus'

export default class extends Controller {
  static targets = [
    'free', 'starter',
    'freeButton', 'starterButton',
    'form', 'cardToken', 'plan', 'submit',
    'hasPaymentMethod', 'paymentMethod', 'paymentPromoCode',
    'newPaymentLink',
  ]

  initialize() {
    this.stripe = Stripe(
      document.querySelector('meta[name="stripe_key"]').getAttribute("content")
    )
    const elements = this.stripe.elements()
    this.card = elements.create('card')
    this.card.mount('#card-element')

    this.card.addEventListener('change', (event) => {
      if (event.error || !event.complete) {
        this.submitTarget.disabled = true
      } else {
        this.submitTarget.disabled = false
      }
    })
  }

  freeClick() {
    this.planTarget.value = this.freeButtonTarget.dataset.plan
    this.freeTarget.classList.add('is-active')
    this.starterTarget.classList.remove('is-active')

    this.hidePaymentFields()
  }

  starterClick() {
    this.planTarget.value = this.starterButtonTarget.dataset.plan
    this.freeTarget.classList.remove('is-active')
    this.starterTarget.classList.add('is-active')

    if (!this.hasPaymentMethod()) {
      this.showPaymentFields()
      this.submitTarget.disabled = true
    }

    this.newPaymentLinkTarget.classList.remove('is-hidden')
  }

  submitForm(e) {
    e.preventDefault()

    this.submitTarget.classList.add('is-loading')

    if (!this.hasPaymentMethod && this.planTarget.value == this.starterButtonTarget.dataset.plan) {
      this.stripe.createToken(this.card).then((res) => {
        if (res.error) {
          this.submitTarget.classList.remove('is-loading')
        } else {
          this.cardTokenTarget.value = res.token.id

          this.formTarget.submit()
        }
      }).catch((err) => {
        this.submitTarget.classList.remove('is-loading')

        throw err
      })
    } else {
      this.formTarget.submit()
    }
  }

  hasPaymentMethod() {
    return this.hasPaymentMethodTarget.value == 'true'
  }

  hidePaymentFields() {
    this.paymentMethodTarget.classList.add('is-hidden')
    this.submitTarget.disabled = false
    if (this.newPaymentLinkTarget) {
      this.newPaymentLinkTarget.classList.add('is-hidden')
    }
  }

  showPaymentFields() {
    this.paymentMethodTarget.classList.remove('is-hidden')
    if (this.newPaymentLinkTarget) {
      this.newPaymentLinkTarget.classList.add('is-hidden')
    }
  }

  showPromoCodeField() {
    this.paymentPromoCodeTarget.classList.remove('is-hidden')
  }
}
