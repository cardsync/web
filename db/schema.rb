# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_30_233500) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "card_condition_price_histories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "card_condition_id", null: false
    t.datetime "date", null: false
    t.integer "price", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["card_condition_id", "date"], name: "card_condition_price_histories_idx", unique: true
    t.index ["card_condition_id"], name: "index_card_condition_price_histories_on_card_condition_id"
  end

  create_table "card_conditions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "card_id", null: false
    t.text "condition", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["card_id"], name: "index_card_conditions_on_card_id"
  end

  create_table "card_sets", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "code", null: false
    t.string "set_type", null: false
    t.date "released_at", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cards", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "card_set_id"
    t.text "scryfall_id", null: false
    t.text "name", null: false
    t.text "type_line", null: false
    t.text "rarity", null: false
    t.integer "multiverseid"
    t.text "number"
    t.text "names", array: true
    t.text "mana_cost"
    t.float "cmc"
    t.text "colors", array: true
    t.text "color_identity", array: true
    t.text "supertypes", array: true
    t.text "types", array: true
    t.text "subtypes", array: true
    t.text "text"
    t.text "flavor"
    t.text "artist"
    t.text "power"
    t.text "toughness"
    t.integer "loyalty"
    t.text "variations", array: true
    t.text "watermark"
    t.text "border"
    t.boolean "reserved"
    t.text "layout"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "photo"
    t.text "collector_number"
    t.text "image"
    t.index ["card_set_id"], name: "index_cards_on_card_set_id"
    t.index ["scryfall_id"], name: "index_cards_on_scryfall_id", unique: true
  end

  create_table "ebay_offers", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.uuid "user_card_condition_id", null: false
    t.text "offer_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "listing_id"
    t.integer "quantity", default: 1, null: false
    t.datetime "refreshed_at"
    t.index ["offer_id"], name: "index_ebay_offers_on_offer_id"
    t.index ["user_card_condition_id"], name: "index_ebay_offers_on_user_card_condition_id"
    t.index ["user_id", "user_card_condition_id", "quantity"], name: "ebay_offers_uniq_idx", unique: true
    t.index ["user_id"], name: "index_ebay_offers_on_user_id"
  end

  create_table "ebay_transaction_histories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_card_condition_id", null: false
    t.integer "quantity", null: false
    t.text "transaction_id", null: false
    t.text "xml", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_card_condition_id"], name: "index_ebay_transaction_histories_on_user_card_condition_id"
  end

  create_table "shopify_user_card_conditions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_card_condition_id", null: false
    t.uuid "user_id", null: false
    t.bigint "shopify_product_variant_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shopify_product_variant_id"], name: "shopify_user_card_condition_shopify_variant_idx", unique: true
    t.index ["user_card_condition_id"], name: "index_shopify_user_card_conditions_on_user_card_condition_id"
    t.index ["user_id", "user_card_condition_id"], name: "shopify_user_card_conditions_user_ucc_idx", unique: true
    t.index ["user_id"], name: "index_shopify_user_card_conditions_on_user_id"
  end

  create_table "shopify_user_card_sets", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "card_set_id", null: false
    t.uuid "user_id", null: false
    t.bigint "shopify_collection_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["card_set_id", "user_id"], name: "index_shopify_user_card_sets_on_card_set_id_and_user_id", unique: true
    t.index ["card_set_id"], name: "index_shopify_user_card_sets_on_card_set_id"
    t.index ["shopify_collection_id"], name: "index_shopify_user_card_sets_on_shopify_collection_id", unique: true
    t.index ["user_id"], name: "index_shopify_user_card_sets_on_user_id"
  end

  create_table "shopify_user_cards", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "card_id", null: false
    t.uuid "user_id", null: false
    t.bigint "shopify_product_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["card_id"], name: "index_shopify_user_cards_on_card_id"
    t.index ["shopify_product_id"], name: "index_shopify_user_cards_on_shopify_product_id", unique: true
    t.index ["user_id", "card_id"], name: "index_shopify_user_cards_on_user_id_and_card_id", unique: true
    t.index ["user_id"], name: "index_shopify_user_cards_on_user_id"
  end

  create_table "tcgplayer_card_conditions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "card_condition_id", null: false
    t.integer "sku_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["card_condition_id", "sku_id"], name: "sku_id_card_condition_id_idx", unique: true
    t.index ["card_condition_id"], name: "index_tcgplayer_card_conditions_on_card_condition_id"
  end

  create_table "user_addresses", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.text "street", null: false
    t.text "building"
    t.text "city", null: false
    t.text "subnational", null: false
    t.text "postal_code"
    t.text "country"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_addresses_on_user_id", unique: true
  end

  create_table "user_card_condition_ebay_api_histories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_card_condition_id", null: false
    t.text "interaction_type", null: false
    t.text "api_request", null: false
    t.text "api_response", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_card_condition_id"], name: "index_ebay_api_histories_user_card_condition"
  end

  create_table "user_card_condition_shopify_api_histories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_card_condition_id", null: false
    t.text "interaction_type", null: false
    t.text "api_request", null: false
    t.text "api_response", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_card_condition_id"], name: "index_shopify_api_histories_user_card_condition"
  end

  create_table "user_card_conditions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.uuid "card_condition_id", null: false
    t.integer "quantity", default: 0, null: false
    t.integer "price", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["card_condition_id"], name: "index_user_card_conditions_on_card_condition_id"
    t.index ["user_id"], name: "index_user_card_conditions_on_user_id"
  end

  create_table "user_card_conditions_delayed_sync", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_card_condition_id", null: false
    t.datetime "sync_at", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sync_at"], name: "index_user_card_conditions_delayed_sync_on_sync_at"
    t.index ["user_card_condition_id"], name: "index_user_card_condition_delayed_user_crd_idx"
  end

  create_table "user_ebays", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.text "auth_token", null: false
    t.datetime "auth_token_expires_at", null: false
    t.text "refresh_token", null: false
    t.datetime "refresh_token_expires_at", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "legacy_auth_token"
    t.datetime "legacy_auth_token_expires_at"
    t.text "template"
    t.text "payment_policy_id"
    t.text "fulfillment_policy_id"
    t.text "return_policy_id"
    t.index ["user_id"], name: "index_user_ebays_on_user_id", unique: true
  end

  create_table "user_shopifies", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.string "shop"
    t.string "api_key"
    t.string "password"
    t.string "shared_secret"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_shopifies_on_user_id", unique: true
  end

  create_table "user_tcgplayers", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.string "access_token", null: false
    t.string "bearer_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_tcgplayers_on_user_id"
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "stripe_customer_id"
    t.text "stripe_subscription_id"
    t.string "uid", null: false
    t.text "first_name"
    t.text "last_name"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["stripe_customer_id"], name: "index_users_on_stripe_customer_id"
    t.index ["stripe_subscription_id"], name: "index_users_on_stripe_subscription_id"
    t.index ["uid"], name: "index_users_on_uid"
  end

  add_foreign_key "card_condition_price_histories", "card_conditions"
  add_foreign_key "card_conditions", "cards", on_delete: :cascade
  add_foreign_key "cards", "card_sets", on_delete: :cascade
  add_foreign_key "ebay_offers", "user_card_conditions", on_delete: :cascade
  add_foreign_key "ebay_offers", "users", on_delete: :cascade
  add_foreign_key "ebay_transaction_histories", "user_card_conditions", on_delete: :cascade
  add_foreign_key "shopify_user_card_conditions", "user_card_conditions", on_delete: :cascade
  add_foreign_key "shopify_user_card_conditions", "users", on_delete: :cascade
  add_foreign_key "shopify_user_card_sets", "card_sets", on_delete: :cascade
  add_foreign_key "shopify_user_card_sets", "users", on_delete: :cascade
  add_foreign_key "shopify_user_cards", "cards", on_delete: :cascade
  add_foreign_key "shopify_user_cards", "users", on_delete: :cascade
  add_foreign_key "tcgplayer_card_conditions", "card_conditions"
  add_foreign_key "user_addresses", "users", on_delete: :cascade
  add_foreign_key "user_card_condition_ebay_api_histories", "user_card_conditions", on_delete: :cascade
  add_foreign_key "user_card_condition_shopify_api_histories", "user_card_conditions", on_delete: :cascade
  add_foreign_key "user_card_conditions", "card_conditions", on_delete: :cascade
  add_foreign_key "user_card_conditions", "users", on_delete: :cascade
  add_foreign_key "user_card_conditions_delayed_sync", "user_card_conditions", on_delete: :cascade
  add_foreign_key "user_ebays", "users", on_delete: :cascade
  add_foreign_key "user_shopifies", "users", on_delete: :cascade
  add_foreign_key "user_tcgplayers", "users", on_delete: :cascade
end
