class AddLegacyAuthTokenAndLegacyAuthTokenExpiresAtToUserEbays < ActiveRecord::Migration[5.2]
  def change
    add_column :user_ebays, :legacy_auth_token, :text
    add_column :user_ebays, :legacy_auth_token_expires_at, :timestamptz
  end
end
