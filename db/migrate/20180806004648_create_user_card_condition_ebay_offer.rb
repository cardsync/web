class CreateUserCardConditionEbayOffer < ActiveRecord::Migration[5.2]
  def change
    create_table :ebay_offers, id: :uuid do |t|
      t.belongs_to :user,
        type: :uuid,
        null: false,
        foreign_key: { on_delete: :cascade },
        index: :true
      t.belongs_to :user_card_condition,
        type: :uuid,
        null: false,
        foreign_key: { on_delete: :cascade },
        index: true
      t.text :offer_id,
        type: :text,
        null: false,
        index: true

      t.timestamps

      t.index [:user_id, :user_card_condition_id], unique: true
    end
  end
end
