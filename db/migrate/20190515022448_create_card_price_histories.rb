class CreateCardPriceHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :card_condition_price_histories, id: :uuid do |t|
      t.belongs_to :card_condition, foreign_key: true, type: :uuid, null: false
      t.datetime :date, null: false
      t.integer :price, null: false

      t.timestamps

      t.index [:card_condition_id, :date], unique: true, name: 'card_condition_price_histories_idx'
    end
  end
end
