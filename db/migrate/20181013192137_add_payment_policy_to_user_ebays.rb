class AddPaymentPolicyToUserEbays < ActiveRecord::Migration[5.2]
  def change
    add_column :user_ebays, :payment_policy_id, :text
  end
end
