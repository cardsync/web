class CreateCardSets < ActiveRecord::Migration[5.2]
  def change
    create_table :card_sets, id: :uuid do |t|
      t.string :name, null: false
      t.string :code, null: false, unique: true
      t.string :set_type, null: false
      t.date :released_at, null: false

      t.timestamps
    end
  end
end
