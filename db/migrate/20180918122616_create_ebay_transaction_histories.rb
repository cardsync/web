class CreateEbayTransactionHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :ebay_transaction_histories, id: :uuid do |t|
      t.belongs_to :user_card_condition, type: :uuid, null: false, foreign_key: { on_delete: :cascade }
      t.integer :quantity, null: false
      t.text :transaction_id, null: false
      t.text :xml, null: false

      t.timestamps
    end
  end
end
