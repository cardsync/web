class TcgplayerUniqueIndex < ActiveRecord::Migration[5.2]
  def change
    add_index :tcgplayer_card_conditions, [:card_condition_id, :sku_id], unique: true, name: :sku_id_card_condition_id_idx
  end
end
