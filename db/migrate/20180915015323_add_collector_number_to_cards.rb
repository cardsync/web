class AddCollectorNumberToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :collector_number, :text
  end
end
