class AddQuantityToEbayOffers < ActiveRecord::Migration[5.2]
  def change
    add_column :ebay_offers, :quantity, :integer, null: false, default: 1
  end
end
