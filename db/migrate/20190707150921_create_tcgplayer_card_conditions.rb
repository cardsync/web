class CreateTcgplayerCardConditions < ActiveRecord::Migration[5.2]
  def change
    create_table :tcgplayer_card_conditions, id: :uuid do |t|
      t.belongs_to :card_condition, foreign_key: true, type: :uuid, null: false
      t.integer :sku_id, null: false

      t.timestamps
    end
  end
end
