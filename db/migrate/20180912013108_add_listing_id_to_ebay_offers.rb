class AddListingIdToEbayOffers < ActiveRecord::Migration[5.2]
  def change
    add_column :ebay_offers, :listing_id, :text
  end
end
