class CreateUserCardConditionEbayApiHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :user_card_condition_ebay_api_histories, id: :uuid do |t|
      t.belongs_to :user_card_condition, type: :uuid, null: false, foreign_key: { on_delete: :cascade }, index: { name: 'index_ebay_api_histories_user_card_condition' }
      t.text :interaction_type, null: false
      t.text :api_request, null: false
      t.text :api_response, null: false

      t.timestamps
    end
  end
end
