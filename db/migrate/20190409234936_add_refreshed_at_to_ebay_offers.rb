class AddRefreshedAtToEbayOffers < ActiveRecord::Migration[5.2]
  def change
    add_column :ebay_offers, :refreshed_at, :timestamptz
  end
end
