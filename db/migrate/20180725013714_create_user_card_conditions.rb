class CreateUserCardConditions < ActiveRecord::Migration[5.2]
  def change
    create_table :user_card_conditions, id: :uuid do |t|
      t.belongs_to :user, type: :uuid, null: false, foreign_key: { on_delete: :cascade }
      t.belongs_to :card_condition, type: :uuid, null: false, foreign_key: { on_delete: :cascade }
      t.integer :quantity, null: false, default: 0
      t.integer :price, null: false, default: 0

      t.timestamps
    end
  end
end
