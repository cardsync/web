class AddStripeCustomerIdToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :stripe_customer_id, :text, index: true
    add_index :users, :stripe_customer_id
    add_column :users, :stripe_subscription_id, :text, index: true
    add_index :users, :stripe_subscription_id
  end
end
