class CreateCardConditions < ActiveRecord::Migration[5.2]
  def change
    create_table :card_conditions, id: :uuid do |t|
      t.belongs_to :card, type: :uuid, null: false, foreign_key: { on_delete: :cascade }
      t.text :condition, null: false

      t.timestamps
    end
  end
end
