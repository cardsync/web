class CreateUserShopifies < ActiveRecord::Migration[6.0]
  def change
    create_table :user_shopifies, id: :uuid do |t|
      t.belongs_to :user, type: :uuid, null: false, foreign_key: { on_delete: :cascade }, index: { unique: true }
      t.string :shop
      t.string :api_key
      t.string :password
      t.string :shared_secret

      t.timestamps
    end

    create_table :shopify_user_card_sets, id: :uuid do |t|
      t.belongs_to :card_set, type: :uuid, null: false, foreign_key: { on_delete: :cascade }
      t.belongs_to :user, type: :uuid, null: false, foreign_key: { on_delete: :cascade }
      t.integer :shopify_collection_id, null: false, limit: 5, index: { unique: true }

      t.timestamps

      t.index [:card_set_id, :user_id], unique: true
    end

    create_table :shopify_user_cards, id: :uuid do |t|
      t.belongs_to :card, type: :uuid, null: false, foreign_key: { on_delete: :cascade }
      t.belongs_to :user, type: :uuid, null: false, foreign_key: { on_delete: :cascade }
      t.integer :shopify_product_id, null: false, limit: 5, index: { unique: true }

      t.timestamps

      t.index [:user_id, :card_id], unique: true
    end

    create_table :shopify_user_card_conditions, id: :uuid do |t|
      t.belongs_to :user_card_condition, type: :uuid, null: false, foreign_key: { on_delete: :cascade }
      t.belongs_to :user, type: :uuid, null: false, foreign_key: { on_delete: :cascade }
      t.integer :shopify_product_variant_id, null: false, limit: 5, index: { unique: true, name: :shopify_user_card_condition_shopify_variant_idx }

      t.timestamps

      t.index [:user_id, :user_card_condition_id], unique: true, name: :shopify_user_card_conditions_user_ucc_idx
    end
  end
end
