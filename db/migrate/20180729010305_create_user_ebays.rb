class CreateUserEbays < ActiveRecord::Migration[5.2]
  def change
    create_table :user_ebays, id: :uuid do |t|
      t.belongs_to :user, type: :uuid, null: false, foreign_key: { on_delete: :cascade }, index: { unique: true }
      t.text :auth_token, null: false
      t.datetime :auth_token_expires_at, null: false
      t.text :refresh_token, null: false
      t.datetime :refresh_token_expires_at, null: false

      t.timestamps
    end
  end
end
