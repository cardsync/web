class CardsUniqueScryfallId < ActiveRecord::Migration[5.2]
  def change
    add_index :cards, [:scryfall_id], unique: true
  end
end
