class AddTemplateToUserEbays < ActiveRecord::Migration[5.2]
  def change
    add_column :user_ebays, :template, :text
  end
end
