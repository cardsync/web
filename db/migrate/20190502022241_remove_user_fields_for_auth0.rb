class RemoveUserFieldsForAuth0 < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :uid, :string, null: false
    add_index :users, :uid

    remove_column :users, :encrypted_password, :string
    remove_column :users, :reset_password_token, :string
    remove_column :users, :reset_password_sent_at, :timestamp
    remove_column :users, :remember_created_at, :timestamp
    remove_column :users, :sign_in_count, :integer
    remove_column :users, :current_sign_in_at, :timestamp
    remove_column :users, :last_sign_in_at, :timestamp
    remove_column :users, :current_sign_in_ip, :string
    remove_column :users, :last_sign_in_ip, :string
    remove_column :users, :admin, :bool
  end
end
