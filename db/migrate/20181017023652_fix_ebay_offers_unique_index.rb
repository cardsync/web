class FixEbayOffersUniqueIndex < ActiveRecord::Migration[5.2]
  def change
    remove_index :ebay_offers, [:user_id, :user_card_condition_id]
    add_index :ebay_offers, [:user_id, :user_card_condition_id, :quantity], unique: true, name: :ebay_offers_uniq_idx
  end
end
