class CreateUserTcgplayers < ActiveRecord::Migration[5.2]
  def change
    create_table :user_tcgplayers, id: :uuid do |t|
      t.belongs_to :user, type: :uuid, null: false, foreign_key: { on_delete: :cascade }
      t.string :access_token, null: false
      t.string :bearer_token

      t.timestamps
    end
  end
end
