class UserCardConditionsDelayedSync < ActiveRecord::Migration[5.2]
  def change
    create_table :user_card_conditions_delayed_sync, id: :uuid do |t|
      t.belongs_to :user_card_condition,
        type: :uuid,
        null: false,
        foreign_key: { on_delete: :cascade },
        index: { name: 'index_user_card_condition_delayed_user_crd_idx' }
      t.datetime :sync_at,
        null: false,
        index: true

      t.timestamps
    end
  end
end
