class CreateUserAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :user_addresses, id: :uuid do |t|
      t.belongs_to :user, type: :uuid, null: false, foreign_key: { on_delete: :cascade }, index: { unique: true }
      t.text :street, null: false
      t.text :building
      t.text :city, null: false
      t.text :subnational, null: false
      t.text :postal_code
      t.text :country, nulL: false

      t.timestamps
    end
  end
end
