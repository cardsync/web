class AddEbaySellerFields < ActiveRecord::Migration[6.0]
  def change
    add_column :user_ebays, :fulfillment_policy_id, :text
    add_column :user_ebays, :return_policy_id, :text
  end
end
