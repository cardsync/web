class TimezoneTimestamps < ActiveRecord::Migration[5.2]
  def change
    change_column :card_conditions, :created_at, :timestamptz
    change_column :card_conditions, :updated_at, :timestamptz
    change_column :card_sets, :created_at, :timestamptz
    change_column :card_sets, :updated_at, :timestamptz
    change_column :cards, :created_at, :timestamptz
    change_column :cards, :updated_at, :timestamptz
    change_column :ebay_offers, :created_at, :timestamptz
    change_column :ebay_offers, :updated_at, :timestamptz
    change_column :ebay_transaction_histories, :created_at, :timestamptz
    change_column :ebay_transaction_histories, :updated_at, :timestamptz
    change_column :user_addresses, :created_at, :timestamptz
    change_column :user_addresses, :updated_at, :timestamptz
    change_column :user_card_condition_ebay_api_histories, :created_at, :timestamptz
    change_column :user_card_condition_ebay_api_histories, :updated_at, :timestamptz
    change_column :user_card_conditions, :created_at, :timestamptz
    change_column :user_card_conditions, :updated_at, :timestamptz
    change_column :user_card_conditions_delayed_sync, :created_at, :timestamptz
    change_column :user_card_conditions_delayed_sync, :updated_at, :timestamptz
    change_column :user_ebays, :created_at, :timestamptz
    change_column :user_ebays, :updated_at, :timestamptz
    change_column :users, :created_at, :timestamptz
    change_column :users, :updated_at, :timestamptz
  end
end
