class CreateCards < ActiveRecord::Migration[5.2]
  def change
    create_table :cards, id: :uuid do |t|
      t.belongs_to :card_set, type: :uuid, foreign_key: { on_delete: :cascade }
      t.text :scryfall_id, null: false, uuid: true
      t.text :name, null: false
      t.text :type_line, null: false
      t.text :rarity, null: false
      t.integer :multiverseid
      t.text :number
      t.text :names, array: true
      t.text :mana_cost
      t.float :cmc
      t.text :colors, array: true
      t.text :color_identity, array: true
      t.text :supertypes, array: true
      t.text :types, array: true
      t.text :subtypes, array: true
      t.text :text
      t.text :flavor
      t.text :artist
      t.text :power
      t.text :toughness
      t.integer :loyalty
      t.text :variations, array: true
      t.text :watermark
      t.text :border
      t.boolean :reserved
      t.text :layout

      t.timestamps
    end
  end
end
