Rails.application.routes.draw do
  get 'heartbeat/index'

  get 'marketplace/settings'

  namespace :ebay do
    resource :token, only: [:show] do
      post :request_grant
      get :save_token

      get :start_fetch_legacy_token
    end
    resource :notification, only: [:show]
    resource :location, only: [:edit, :update]
    resource :fulfillment, only: [:edit, :update]
    resource :payment, only: [:create, :edit, :update]
    resource :return, only: [:edit, :update]
    resource :template, only: [:edit, :update]
    resource :usage, only: [:show]

    post 'notification'
  end

  namespace :tcgplayer do
    resource :token, only: [:show, :create]
  end

  namespace :shopify do
    resource :token, only: [:show, :create]

    post 'notification'
  end

  resources :user_card_condition, only: [:index, :update, :show]

  post 'stripe', to: 'stripe#webhook'

  get 'privacy', to: 'static#privacy'
  get 'terms', to: 'static#terms'
  get 'product', to: 'static#product'
  get 'pricing', to: 'static#pricing'
  get 'home/index'

  resource :subscription, only: [:show, :update]

  get '/auth/auth0/callback' => 'auth0#callback'
  get '/auth/failure' => 'auth0#failure'
  get '/logout' => 'logout#logout'

  get '/heartbeat', to: 'heartbeat#index'

  get '/404', to: 'static#not_found'
  get '/500', to: 'static#internal_server_error', as: 'internal_server_error'

  root to: "home#index"
end
