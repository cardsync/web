FROM ruby:2.7.1-alpine

RUN bundle config build.google-protobuf --with-cflags=-D__va_copy=va_copy
RUN bundle config --global frozen 1

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ENV RAILS_ENV production
ENV RAILS_SERVE_STATIC_FILES true
ENV RAILS_LOG_TO_STDOUT true

COPY Gemfile Gemfile.lock ./

RUN apk update && apk add -U build-base libxml2-dev libxslt-dev postgresql-dev nodejs

#RUN bundle config build.nokogiri --use-system-libraries
RUN gem install pkg-config -v "~> 1.1"
RUN BUNDLE_FORCE_RUBY_PLATFORM=1 bundle install --without development test

COPY . .

RUN apk add libc6-compat tzdata yarn bash

RUN dotenv -f .ci.env bundle exec rake assets:precompile

RUN apk del nodejs

CMD ["puma", "-C", "config/puma.rb"]
